-- ----------------------------
-- 新增用户详情[user_info_detail]-增删查改菜单和权限
-- ----------------------------
-- 新增菜单
INSERT INTO `sys_menu` (`create_time`, `i_frame`, `name`, `component`, `pid`, `sort`, `icon`, `path`) VALUES (NOW(), 0, '用户详情管理', 'system/user/index', '1', '2', 'peoples', 'user');

-- 管理员分配权限
INSERT INTO `sys_roles_menus` (`menu_id`, `role_id`) VALUES ((SELECT s._id from (SELECT id as _id from sys_menu WHERE name = '用户详情管理') as s), '1');

-- 新增权限编码
-- all
INSERT INTO `sys_permission` (`alias`, `create_time`, `name`, `pid`) VALUES ('用户详情管理', NOW(), 'USERINFODETAIL_ALL', (SELECT s._id from (SELECT id as _id from sys_permission WHERE name = 'SYS_ALL') as s));
-- 创建
INSERT INTO `sys_permission` (`alias`, `create_time`, `name`, `pid`) VALUES ('用户详情创建', NOW(), 'USERINFODETAIL_CREATE', (SELECT s._id from (SELECT id as _id from sys_permission WHERE name = 'USERINFODETAIL_ALL') as s));
-- 删除
INSERT INTO `sys_permission` (`alias`, `create_time`, `name`, `pid`) VALUES ('用户详情删除', NOW(), 'USERINFODETAIL_DELETE', (SELECT s._id from (SELECT id as _id from sys_permission WHERE name = 'USERINFODETAIL_ALL') as s));
INSERT INTO `sys_permission` (`alias`, `create_time`, `name`, `pid`) VALUES ('用户详情查询', NOW(), 'USERINFODETAIL_SELECT', (SELECT s._id from (SELECT id as _id from sys_permission WHERE name = 'USERINFODETAIL_ALL') as s));
INSERT INTO `sys_permission` (`alias`, `create_time`, `name`, `pid`) VALUES ('用户详情修改', NOW(), 'USERINFODETAIL_EDIT', (SELECT s._id from (SELECT id as _id from sys_permission WHERE name = 'USERINFODETAIL_ALL') as s));
