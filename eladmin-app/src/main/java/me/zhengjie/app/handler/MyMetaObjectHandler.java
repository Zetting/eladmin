//package me.zhengjie.app.handler;
//
//import cn.hutool.core.util.IdUtil;
//import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.ibatis.reflection.MetaObject;
//import org.springframework.stereotype.Component;
//
//import java.util.Date;
//
///**
// * @author: linzetian
// * @date:2019/6/27
// */
//@Component
//@Slf4j
//public class MyMetaObjectHandler implements MetaObjectHandler {
//
//    @Override
//    public void insertFill(MetaObject metaObject) {
//        log.debug("start insert fill ....");
//        this.setInsertFieldValByName("id", IdUtil.fastSimpleUUID(), metaObject);
//        this.setInsertFieldValByName("createTime", new Date(), metaObject);
//    }
//
//    @Override
//    public void updateFill(MetaObject metaObject) {
//        log.debug("start update fill ....");
//        this.setUpdateFieldValByName("updateTime", new Date(), metaObject);
//    }
//}
