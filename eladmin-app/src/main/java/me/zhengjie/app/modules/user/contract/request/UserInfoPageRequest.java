package me.zhengjie.app.modules.user.contract.request;

import lombok.Data;
import javax.persistence.*;
import io.swagger.annotations.ApiModelProperty;
import me.zhengjie.base.BaseRequest;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* 用户信息-分页请求
* @author zet
* @date 2019-07-03
*/
@Data
public class UserInfoPageRequest extends BaseRequest {

    @ApiModelProperty("主键Id")
    private String id;

    @ApiModelProperty("登陆名")
    private String loginName;

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("登陆密码")
    private String password;

    @ApiModelProperty("手机")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("头像")
    private String avatar;

    @ApiModelProperty("性别")
    private Integer sex;

    @ApiModelProperty("0无效1有效")
    private Integer valid;

    @ApiModelProperty("最后登录时间")
    private Timestamp lastLoginTime;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("更新时间")
    private Timestamp updateTime;

    @ApiModelProperty("国际电话国家代码，例如中国：86")
    private String zoneCode;
}
