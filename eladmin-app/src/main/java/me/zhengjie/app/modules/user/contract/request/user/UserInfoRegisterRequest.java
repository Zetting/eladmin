package me.zhengjie.app.modules.user.contract.request.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import me.zhengjie.base.BaseRequest;
import me.zhengjie.contants.RegexConstant;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 注册请求
 *
 * @author: zet
 * @date:2019/7/5
 */
@Data
public class UserInfoRegisterRequest extends BaseRequest {
    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("登陆密码")
    private String password;

    @NotBlank
    @Pattern(regexp = RegexConstant.MOBILE, message = "手机号码不正确")
    @ApiModelProperty(value = "手机",required = true)
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("头像")
    private String avatar;

    @ApiModelProperty("性别")
    private Integer sex;

    @ApiModelProperty("国际电话国家代码，例如中国：86")
    private String zoneCode;

    @NotBlank
    @Pattern(regexp = RegexConstant.SMS_CODE, message = "验证码不正确")
    @ApiModelProperty(value = "验证码", required = true)
    private String smsCode;
}
