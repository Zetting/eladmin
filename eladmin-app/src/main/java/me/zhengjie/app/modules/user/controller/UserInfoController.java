package me.zhengjie.app.modules.user.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhengjie.app.modules.user.contract.request.UserInfoCreateRequest;
import me.zhengjie.app.modules.user.contract.request.UserInfoLoginRequest;
import me.zhengjie.app.modules.user.contract.request.UserInfoUpdateRequest;
import me.zhengjie.app.modules.user.contract.request.user.UserInfoRegisterRequest;
import me.zhengjie.app.modules.user.contract.response.UserInfoLoginResponse;
import me.zhengjie.app.modules.user.contract.response.user.UserInfoRegisterResponse;
import me.zhengjie.app.modules.user.service.UserInfoService;
import me.zhengjie.base.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 用户信息-控制层
 *
 * @author zet
 * @date 2019-07-03
 */
@Api(tags = "用户信息")
@RestController
@RequestMapping("user/userInfo")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    @ApiOperation("注册")
    @PostMapping(value = "/{version}/register")
    public Response<UserInfoRegisterResponse> register(@RequestBody UserInfoRegisterRequest request,
                                                       @PathVariable String version) {
        return userInfoService.register(request);
    }


    @ApiOperation("登陆")
    @PostMapping(value = "/{version}/login")
    public Response<UserInfoLoginResponse> login(@RequestBody UserInfoLoginRequest request,
                                                 @PathVariable String version) {
        return userInfoService.login(request);
    }

}
