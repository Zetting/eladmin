package me.zhengjie.app.modules.user.contract.request.user;

import lombok.Data;
import javax.persistence.*;
import io.swagger.annotations.ApiModelProperty;
import me.zhengjie.base.BaseRequest;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* 用户详情-分页请求
* @author zet
* @date 2019-07-05
*/
@Data
public class UserInfoDetailPageRequest extends BaseRequest {

    @ApiModelProperty("主键Id")
    private String id;

    @ApiModelProperty("用户Id，user_info.id")
    private String userId;

    @ApiModelProperty("真实姓名")
    private String realName;

    @ApiModelProperty("常住地址")
    private String address;

    @ApiModelProperty("生日")
    private Timestamp birthday;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("更新时间")
    private Timestamp updateTime;
}
