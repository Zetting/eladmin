package me.zhengjie.app.modules.user.contract.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户登陆响应
 *
 * @author: zet
 * @date:2019/7/3
 */
@Data
public class UserInfoLoginResponse {
    @ApiModelProperty("accessToken")
    private String accessToken;

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("用户姓名")
    private String userName;
}
