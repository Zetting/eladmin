package me.zhengjie.app.modules.user.service.impl;

import me.zhengjie.base.CommonPage;
import com.github.pagehelper.PageHelper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.zhengjie.entity.user.UserInfoDetail;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.app.modules.user.service.UserInfoDetailService;
import me.zhengjie.app.modules.user.dao.UserInfoDetailDao;
import me.zhengjie.app.modules.user.contract.request.user.UserInfoDetailPageRequest;
import me.zhengjie.app.modules.user.contract.request.user.UserInfoDetailCreateRequest;
import me.zhengjie.app.modules.user.contract.request.user.UserInfoDetailUpdateRequest;
import me.zhengjie.app.modules.user.contract.response.user.UserInfoDetailGetResponse;
import me.zhengjie.app.modules.user.contract.response.user.UserInfoDetailPageResponse;
import java.util.Optional;
import java.util.List;
import cn.hutool.core.util.IdUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
import java.util.List;

/**
* 用户详情-服务实现类
*
* @author zet
* @date 2019-07-05
*/
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class UserInfoDetailServiceImpl extends ServiceImpl<UserInfoDetailDao, UserInfoDetail> implements UserInfoDetailService {
    @Autowired
    private UserInfoDetailDao  userInfoDetailDao;

    @Override
    public CommonPage<UserInfoDetailPageResponse>  listPages(UserInfoDetailPageRequest request, Pageable pageable){
        PageHelper.startPage(pageable.getPageNumber(), pageable.getPageSize());
        List<UserInfoDetailPageResponse> list = userInfoDetailDao.listPages(request);
        return CommonPage.restPage(list);
    }

    @Override
    public UserInfoDetailGetResponse findById(String id) {
        UserInfoDetailGetResponse response = new UserInfoDetailGetResponse();
        UserInfoDetail userInfoDetail =  userInfoDetailDao.selectById(id);
        BeanUtils.copyProperties(userInfoDetail, response);
        return response;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(UserInfoDetailCreateRequest request) {
        UserInfoDetail userInfoDetail = new UserInfoDetail();
        BeanUtils.copyProperties(request, userInfoDetail);
        userInfoDetailDao.insert(userInfoDetail);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(UserInfoDetailUpdateRequest request) {
        UserInfoDetail userInfoDetail = new UserInfoDetail();
        BeanUtils.copyProperties(request, userInfoDetail);
        userInfoDetailDao.updateById(userInfoDetail);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String id) {
        userInfoDetailDao.deleteById(id);
    }
}
