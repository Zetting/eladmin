package me.zhengjie.app.modules.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import me.zhengjie.app.modules.user.contract.request.UserInfoCreateRequest;
import me.zhengjie.app.modules.user.contract.request.UserInfoLoginRequest;
import me.zhengjie.app.modules.user.contract.request.UserInfoUpdateRequest;
import me.zhengjie.app.modules.user.contract.request.user.UserInfoRegisterRequest;
import me.zhengjie.app.modules.user.contract.response.UserInfoGetResponse;
import me.zhengjie.app.modules.user.contract.response.UserInfoLoginResponse;
import me.zhengjie.app.modules.user.contract.response.user.UserInfoRegisterResponse;
import me.zhengjie.app.modules.user.dao.UserInfoDao;
import me.zhengjie.app.modules.user.service.UserInfoService;
import me.zhengjie.base.Response;
import me.zhengjie.entity.user.UserInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户信息-服务实现类
 *
 * @author zet
 * @date 2019-07-03
 */
@Slf4j
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class UserInfoServiceImpl extends ServiceImpl<UserInfoDao, UserInfo> implements UserInfoService {

    @Autowired
    private UserInfoDao userInfoDao;

    @Override
    public Response<UserInfoRegisterResponse> register(UserInfoRegisterRequest request) {
        //校验短信验证码
        //校验是否注册

        //注册
        return null;
    }

    @Override
    public Response<UserInfoLoginResponse> login(UserInfoLoginRequest request) {
        log.info("[登录接口] request={}", JSON.toJSONString(request));
        return null;
    }

    @Override
    public UserInfoGetResponse findById(String id) {
        UserInfoGetResponse response = new UserInfoGetResponse();
        UserInfo userInfo = userInfoDao.selectById(id);
        BeanUtils.copyProperties(userInfo, response);
        return response;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(UserInfoCreateRequest request) {
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(request, userInfo);
        userInfoDao.insert(userInfo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(UserInfoUpdateRequest request) {
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(request, userInfo);
        userInfoDao.updateById(userInfo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String id) {
        userInfoDao.deleteById(id);
    }
}
