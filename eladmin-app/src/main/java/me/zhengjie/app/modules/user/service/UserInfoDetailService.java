package me.zhengjie.app.modules.user.service;

import me.zhengjie.base.CommonPage;
import com.baomidou.mybatisplus.extension.service.IService;
import me.zhengjie.entity.user.UserInfoDetail;
import me.zhengjie.app.modules.user.contract.request.user.UserInfoDetailPageRequest;
import me.zhengjie.app.modules.user.contract.request.user.UserInfoDetailCreateRequest;
import me.zhengjie.app.modules.user.contract.request.user.UserInfoDetailUpdateRequest;
import me.zhengjie.app.modules.user.contract.response.user.UserInfoDetailGetResponse;
import me.zhengjie.app.modules.user.contract.response.user.UserInfoDetailPageResponse;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
* 用户详情-服务类
* @author zet
* @date 2019-07-05
*/
public interface UserInfoDetailService extends IService<UserInfoDetail>{
    /**
    * queryAll 分页
    * @param request
    * @param pageable
    * @return
    */
    CommonPage<UserInfoDetailPageResponse> listPages(UserInfoDetailPageRequest request, Pageable pageable);


    /**
     * findById
     * @param id
     * @return
     */
    UserInfoDetailGetResponse findById(String id);

    /**
     * create
     * @param request
     * @return
     */
    void create(UserInfoDetailCreateRequest request);

    /**
     * update
     * @param request
     */
    void update(UserInfoDetailUpdateRequest request);

    /**
     * delete
     * @param id
     */
    void delete(String id);
}
