package me.zhengjie.app.modules.user.dao;

import me.zhengjie.app.modules.user.contract.request.user.UserInfoDetailPageRequest;
import me.zhengjie.app.modules.user.contract.response.user.UserInfoDetailPageResponse;
import me.zhengjie.entity.user.UserInfoDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;


/**
* 用户详情-持久层
*
* @author zet
* @date 2019-07-05
*/
public interface UserInfoDetailDao extends BaseMapper<UserInfoDetail> {
    List<UserInfoDetailPageResponse> listPages(UserInfoDetailPageRequest query);
}
