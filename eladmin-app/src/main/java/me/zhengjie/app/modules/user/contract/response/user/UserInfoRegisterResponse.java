package me.zhengjie.app.modules.user.contract.response.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 注册响应
 *
 * @author: zet
 * @date:2019/7/5
 */
@Data
public class UserInfoRegisterResponse {
    @ApiModelProperty("accessToken")
    private String accessToken;

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("用户姓名")
    private String userName;
}
