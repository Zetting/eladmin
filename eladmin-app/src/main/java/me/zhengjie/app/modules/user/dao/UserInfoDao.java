package me.zhengjie.app.modules.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.zhengjie.entity.user.UserInfo;


/**
* 用户信息-持久层
*
* @author zet
* @date 2019-07-03
*/
public interface UserInfoDao extends BaseMapper<UserInfo> {

}
