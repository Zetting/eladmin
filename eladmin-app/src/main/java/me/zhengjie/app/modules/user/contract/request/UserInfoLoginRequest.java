package me.zhengjie.app.modules.user.contract.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import me.zhengjie.base.BaseRequest;
import me.zhengjie.contants.RegexConstant;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;

/**
 * 用户登陆请求
 *
 * @author zet
 * @date 2019-07-03
 */
@Data
public class UserInfoLoginRequest extends BaseRequest {
    @NotBlank
    @ApiModelProperty(value = "登录方式；\n " +
            "ACCOUNT：账号方式 phone,password必传 \n " +
            "PHONE：手机登录方式 phone,verifyCode必传\n", required = true)
    private String loginMode;

    @NotBlank
    @Pattern(regexp = RegexConstant.MOBILE, message = "手机号码不正确")
    @ApiModelProperty(value = "手机号码")
    private String phone;

    @Pattern(regexp = RegexConstant.SMS_CODE, message = "验证码不正确")
    @ApiModelProperty(value = "验证码")
    private String verifyCode;

    @ApiModelProperty(value = "登陆密码")
    private String password;
}
