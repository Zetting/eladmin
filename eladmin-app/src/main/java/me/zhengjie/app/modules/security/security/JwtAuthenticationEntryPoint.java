package me.zhengjie.app.modules.security.security;

import cn.hutool.json.JSONUtil;
import me.zhengjie.base.FailedResponse;
import me.zhengjie.base.Response;
import me.zhengjie.base.ResponseCode;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    private static final long serialVersionUID = -8970718410437077606L;

    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException {
        /**
         * 当用户尝试访问安全的REST资源而不提供任何凭据时，将调用此方法发送401 响应
         */
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().println(JSONUtil.parse(new FailedResponse(ResponseCode.UN_LOGIN.getCode()
                ,ResponseCode.UN_LOGIN.getMessage())));
        response.getWriter().flush();
    }
}
