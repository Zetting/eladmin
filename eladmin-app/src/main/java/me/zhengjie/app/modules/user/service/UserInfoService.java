package me.zhengjie.app.modules.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import me.zhengjie.app.modules.user.contract.request.UserInfoCreateRequest;
import me.zhengjie.app.modules.user.contract.request.UserInfoLoginRequest;
import me.zhengjie.app.modules.user.contract.request.UserInfoUpdateRequest;
import me.zhengjie.app.modules.user.contract.request.user.UserInfoRegisterRequest;
import me.zhengjie.app.modules.user.contract.response.UserInfoGetResponse;
import me.zhengjie.app.modules.user.contract.response.UserInfoLoginResponse;
import me.zhengjie.app.modules.user.contract.response.user.UserInfoRegisterResponse;
import me.zhengjie.base.Response;
import me.zhengjie.entity.user.UserInfo;

/**
 * 用户信息-服务类
 *
 * @author zet
 * @date 2019-07-03
 */
public interface UserInfoService extends IService<UserInfo> {
    /**
     * 注册
     *
     * @param request
     */
    Response<UserInfoRegisterResponse> register(UserInfoRegisterRequest request);

    /**
     * 登录
     *
     * @param request
     */
    Response<UserInfoLoginResponse> login(UserInfoLoginRequest request);

    /**
     * findById
     *
     * @param id
     * @return
     */
    UserInfoGetResponse findById(String id);

    /**
     * create
     *
     * @param request
     * @return
     */
    void create(UserInfoCreateRequest request);

    /**
     * update
     *
     * @param request
     */
    void update(UserInfoUpdateRequest request);

    /**
     * delete
     *
     * @param id
     */
    void delete(String id);
}
