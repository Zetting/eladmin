package me.zhengjie.app.configureration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author miemie
 * @since 2018-08-10
 */
@Configuration
@MapperScan(basePackages = {"me.zhengjie.app.modules.*.dao"})
@EnableJpaRepositories(basePackages = {"me.zhengjie.*"})
@EntityScan(basePackages = {"me.zhengjie.*"})
@ComponentScan(basePackages = {"me.zhengjie.*"})
public class ScanConfigureration {

}
