package me.zhengjie.app.action.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Api安全认证直接通过
 * Created by zet on 2017/6/14.
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RUNTIME)
@Inherited
public @interface PassApiAuth {

}
