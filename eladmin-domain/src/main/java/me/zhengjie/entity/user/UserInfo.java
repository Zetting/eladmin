package me.zhengjie.entity.user;

import lombok.Data;
import javax.persistence.*;
import io.swagger.annotations.ApiModelProperty;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* 用户信息-实体
* @author zet
* @date 2019-07-03
*/
@Data
@Table(name="user_info")
public class UserInfo implements Serializable {

    @Id
    @ApiModelProperty("主键Id")
    private Long id;

    @ApiModelProperty("登陆名")
    private String loginName;

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("登陆密码")
    private String password;

    @ApiModelProperty("手机")
    private String phone;

    @ApiModelProperty("头像")
    private String avatar;

    @ApiModelProperty("性别")
    private Integer sex;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("0无效1有效")
    private Integer valid;

    @ApiModelProperty("最后登录时间")
    private Timestamp lastLoginTime;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("更新时间")
    private Timestamp updateTime;

    @ApiModelProperty("更新时间")
    private Timestamp lastPasswordResetDate;

    @ApiModelProperty("国际电话国家代码，例如中国：86")
    private String zoneCode;
}
