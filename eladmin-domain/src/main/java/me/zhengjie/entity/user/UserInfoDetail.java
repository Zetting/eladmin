package me.zhengjie.entity.user;

import lombok.Data;
import javax.persistence.*;
import io.swagger.annotations.ApiModelProperty;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* 用户详情-实体
* @author zet
* @date 2019-07-05
*/
@Data
@Table(name="user_info_detail")
public class UserInfoDetail implements Serializable {

    @Id
    @ApiModelProperty("主键Id")
    private String id;

    @ApiModelProperty("用户Id，user_info.id")
    private String userId;

    @ApiModelProperty("真实姓名")
    private String realName;

    @ApiModelProperty("常住地址")
    private String address;

    @ApiModelProperty("生日")
    private Timestamp birthday;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("更新时间")
    private Timestamp updateTime;
}
