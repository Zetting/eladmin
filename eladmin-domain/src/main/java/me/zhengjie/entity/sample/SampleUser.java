package me.zhengjie.entity.sample;

import lombok.Data;
import javax.persistence.*;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.io.Serializable;

/**
* 示例-实体
* @author zet
* @date 2019-07-05
*/
@Data
@Table(name="sample_user")
public class SampleUser implements Serializable {

    @Id
    @ApiModelProperty("主键Id")
    private String id;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("更新时间")
    private Date updateTime;

    @ApiModelProperty("描述")
    private String description;

    @ApiModelProperty("详细描述")
    private String detailDescription;

    @ApiModelProperty("用户名")
    private String username;
}
