package me.zhengjie.contants;


/**
 * 正则表达式常量类
 *
 * @author Zet
 * @date 2019/04/04 12:13
 */
public class RegexConstant {
     private RegexConstant(){}

    /**
     * 0-1 数值限制
     */
    public static final String ZERO_TO_ONE = "0|1";

    /**
     * 1-2 数值限制
     */
    public static final String ONE_TO_TWO = "1|2";

    /**
     * url正则
     */
    public static final String URL = "^(http|https):\\/\\/[\\w\\-]+(\\.[\\w\\-]+)+([\\w\\-\\.,@?^=%&:!\\/~\\+#]*[\\w\\-\\@?^=%&!\\/~\\+#])?$";

    /**
     * 中文正则
     */
    public static final String CHINESE = "^[\u4e00-\u9fa5]*$";

    /**
     * 身份证正则
     */
    public static final String ID_CARD = "^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([0-9]|X|x)$";

    /**
     * 手机号码正则
     */
    public static final String MOBILE = "^1(3|4|5|7|8|9)\\d{9}$";

    /**
     * 金额正则
     */
    public static final String MONEY = "(^[1-9]([0-9]+)?(\\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\\.[0-9]([0-9])?$)";

    /**
     * 平台类型正则
     */
    public static final String PLATFORM_TYPE = "IOS|ANDROID";

    /**
     * 日期正则
     */
    public static final String DATE_YYYY_MM = "^[1-9][0-9]{3}-[0-1][0-9]$";

    /**
     * 客户姓名正则
     */
    public static final String CUSTOMER_NAME = "^[a-zA-Z\u4e00-\u9fa5\\s]{2,20}$";

    /**
     * 密码正则
     */
    public static final String SECRET_KEY_REG = "^[0-9A-Za-z]{6,18}$";

    /**
     * 页码正则
     */
    public static final String PAGE_NUM = "^[1-9]\\d*$";

    /**
     * s是否是11位数字
     */
    public static final String NUMBER_11 = "^\\d{11}$";

    /**
     * 200个字符内
     */
    public static final String FEEDCONTENT_SIZE = "^[\\s\\S]{1,200}$";

    /**
     * 数字正则
     */
    public static final String DIGIT = "\\d+";

    /**
     * 实名正则：中文，最多20个字符
     */
    public static final String REAL_NAME = "[\u4e00-\u9fa5]{1,20}";

    /**
     * 日期正则
     */
    public static final String DATE_YYYY_MM_DD = "^[1-9][0-9]{3}-[0-1][0-9]-[0-3][0-9]$";

    /**
     * 银行名称正则
     */
    public static final String BANK_NAME = "[\u4e00-\u9fa5]{2,20}";


    public static final String BANK_LIMIT = "|^[0-9\u4e00-\u9fa5]{1,20}$";

    /**
     * 短信验证码
     */
    public static final String SMS_CODE = "[0-9]{6}";

    /**
     * 经度： -180.0～+180.0（整数部分为0～180，必须输入1到6位小数），可以为空值
     */
    public static final String LONGITUDE = "|^[\\-\\+]?(0?\\d{1,2}\\.\\d{1,6}|1[0-7]?\\d{1}\\.\\d{1,6}|180\\.0{1,6})$";

    /**
     * 纬度： -90.0～+90.0（整数部分为0～90，必须输入1到6位小数），可以为空值
     */
    public static final String LATITUDE = "|^[\\-\\+]?([0-8]?\\d{1}\\.\\d{1,6}|90\\.0{1,6})$";

    /**
     * 价格格式：正小数，正整数，0
     */
    public static final String PRICE_FORMAT = "^[1-9]\\d*\\.\\d+$|^0\\.\\d+$|^[1-9]\\d*$|^0$";


    /**
     * 排序(1-99)
     */
    public static final String ONE_TO_NINETY_NINE = "^[1-9]{1}[0-9]?$";

    /**
     * 企业名称
     */
    public static final String ENTERPRISE_NAME = "^[\\S]{1,40}$";

    /**
     * 企业对公账号
     */
    public static final String BANK_CARD_NO = "^\\d{1,25}$";

    /**
     * 开户银行许可证
     */
    public static final String BANK_LICENSE = "[0-9a-zA-Z]{1,14}";

    /**
     * 统一社会信用代码
     */
    public static final String UNIFIED_CODE = "[0-9a-zA-Z]{1,18}";
    /**
     * 企业法人
     */
    public static final String LEGAL = "[\\S]{1,15}";
}
