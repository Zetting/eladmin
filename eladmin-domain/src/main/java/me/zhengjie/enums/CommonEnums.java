package me.zhengjie.enums;

import lombok.Getter;

/**
 * 公共枚举类
 *
 * @author: zet
 * @date:2019/7/3
 */
public class CommonEnums {


    /**
     * Boolean 枚举
     */
    public static enum BooleanEnum {
        TURE(1),
        FALSE(0);
        @Getter
        private Integer value;

        BooleanEnum(Integer value) {
            this.value = value;
        }
    }
}
