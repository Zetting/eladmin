CREATE TABLE `user_info` (
  `id` varchar(32) NOT NULL COMMENT ''主键Id'',
  `login_name` varchar(30) DEFAULT NULL COMMENT ''登陆名'',
  `nick_name` varchar(25) DEFAULT NULL COMMENT ''昵称'',
  `password` varchar(50) NOT NULL COMMENT ''登陆密码'',
  `phone` varchar(11) NOT NULL COMMENT ''手机'',
  `email` varchar(30) DEFAULT NULL COMMENT ''邮箱'',
  `avatar` varchar(200) NOT NULL COMMENT ''头像'',
  `sex` int(1) DEFAULT NULL COMMENT ''性别'',
  `valid` int(2) NOT NULL DEFAULT ''1'' COMMENT ''0无效1有效'',
  `last_login_time` datetime NOT NULL COMMENT ''最后登录时间'',
  `create_time` datetime DEFAULT NULL COMMENT ''创建时间'',
  `update_time` datetime DEFAULT NULL COMMENT ''更新时间'',
  `last_password_reset_date` datetime DEFAULT NULL COMMENT ''密码最后的修改时间'',
  `zone_code` varchar(6) NOT NULL COMMENT ''国际电话国家代码，例如中国：86'',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mobilePhone` (`phone`) USING BTREE,
  UNIQUE KEY `name` (`login_name`) USING BTREE,
   UNIQUE KEY `email` (`email`) USING BTREE,
  KEY `ix_CREATE_TIME_SHOP_MASTER_ID` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=''用户信息表'';


CREATE TABLE `user_info_detail` (
  `id` varchar(32) NOT NULL COMMENT ''主键Id'',
  `user_id` varchar(32) DEFAULT NULL COMMENT ''用户Id，user_info.id'',
  `real_name` varchar(30) DEFAULT NULL COMMENT ''真实姓名'',
  `address` varchar(100) DEFAULT NULL COMMENT ''常住地址'',
  `birthday` date DEFAULT NULL COMMENT ''生日'',
  `create_time` datetime DEFAULT NULL COMMENT ''创建时间'',
  `update_time` datetime DEFAULT NULL COMMENT ''更新时间'',
  PRIMARY KEY (`id`),
  KEY `FK74oercqp67apreq726psydf9u` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=''用户详情表'';
