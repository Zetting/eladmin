package me.zhengjie.action.aop;

import me.zhengjie.base.BaseRequest;
import me.zhengjie.base.ResponseCode;
import me.zhengjie.exception.ServerException;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import javax.validation.groups.Default;
import java.util.Iterator;
import java.util.Set;

/**
 * controller 层入参校验切面
 *
 * @Author linzetian
 * @Date 2017/11/15 21:50
 **/
@Order(2)
@Component
@Aspect
public class ValidatorAspect {
    @Resource
    private LocalValidatorFactoryBean localValidatorFactoryBean;

    public ValidatorAspect() {
    }

    /**
     * 切入点
     */
    @Pointcut(
            "@annotation(org.springframework.web.bind.annotation.RequestMapping)" +
                    "||@annotation(org.springframework.web.bind.annotation.PostMapping)" +
                    "||@annotation(org.springframework.web.bind.annotation.PutMapping)" +
                    "||@annotation(org.springframework.web.bind.annotation.DeleteMapping)" +
                    "||@annotation(org.springframework.web.bind.annotation.GetMapping)"
    )
    private void parameterPointCut() {
    }

    /**
     * 处理
     *
     * @param joinPoint
     * @param request
     */
    @Before("parameterPointCut() && args(request,..)")
    public void validateParameter(JoinPoint joinPoint, BaseRequest request) {
        Set<ConstraintViolation<BaseRequest>> validErrors = this.localValidatorFactoryBean.validate(request, new Class[]{Default.class});
        Iterator iterator = validErrors.iterator();
        StringBuilder errorMsg = new StringBuilder();

        while (iterator.hasNext()) {
            ConstraintViolation constraintViolation = (ConstraintViolation) iterator.next();
            String tagMessage = constraintViolation.getMessageTemplate();
            String error = "";
            if (StringUtils.isNotBlank(tagMessage) && tagMessage.startsWith("{")) {
                error = constraintViolation.getPropertyPath() + ":" + constraintViolation.getMessage();
            } else {
                error = constraintViolation.getMessage();
            }
            errorMsg.append(iterator.hasNext() ? error + "; " : error);
        }

        if (!validErrors.isEmpty()) {
            throw new ServerException(ResponseCode.PARAM_ILLEGAL, errorMsg.toString());
        }

    }
}
