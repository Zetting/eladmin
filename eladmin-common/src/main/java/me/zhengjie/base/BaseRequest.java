package me.zhengjie.base;

import lombok.Data;

/**
 * 基础请求基类
 *
 * @author: zet
 * @date: 2018/8/22 7:15
 */
@Data
public class BaseRequest {
}
