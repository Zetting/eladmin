package me.zhengjie.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 通用返回对象
 *
 * @author cailanfresh
 * @date 2019/4/19
 */
@Data
public class Response<T> {
    @ApiModelProperty("响应码")
    private long code;

    @ApiModelProperty("响应消息")
    private String message;

    @ApiModelProperty("响应数据")
    private T data;

    /**
     * 成功返回结果
     */
    public static <T> Response<T> success() {
        return new Response<>(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getMessage(), null);
    }

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     */
    public static <T> Response<T> success(T data) {
        return new Response<>(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getMessage(), data);
    }

    /**
     * 成功返回结果
     *
     * @param data    获取的数据
     * @param message 提示信息
     */
    public static <T> Response<T> success(T data, String message) {
        return new Response<>(ResponseCode.SUCCESS.getCode(), message, data);
    }

    /**
     * 失败返回结果
     *
     * @param code 错误码
     */
    public static <T> Response<T> failed(ResponseCode code) {
        return new Response<>(code.getCode(), code.getMessage(), null);
    }

    /**
     * 失败返回结果
     *
     * @param code 错误码
     */
    public static <T> Response<T> failed(ResponseCode code, String message) {
        return new Response<>(code.getCode(), message, null);
    }

    /**
     * 失败返回结果
     *
     * @param message 提示信息
     */
    public static <T> Response<T> failed(String message) {
        return new Response<>(ResponseCode.FAILED.getCode(), message, null);
    }

    /**
     * 失败返回结果
     */
    public static <T> Response<T> failed() {
        return failed(ResponseCode.FAILED);
    }

    /**
     * 参数验证失败返回结果
     */
    public static <T> Response<T> validateFailed() {
        return failed(ResponseCode.PARAM_ILLEGAL);
    }

    /**
     * 参数验证失败返回结果
     *
     * @param message 提示信息
     */
    public static <T> Response<T> validateFailed(String message) {
        return new Response<>(ResponseCode.PARAM_ILLEGAL.getCode(), message, null);
    }

    protected Response() {
    }

    protected Response(long code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

}
