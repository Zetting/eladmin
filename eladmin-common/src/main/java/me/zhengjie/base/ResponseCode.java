package me.zhengjie.base;

import lombok.Getter;
import lombok.Setter;

/**
 * 基础响应码
 *
 * @Author: zetting
 * @Date: 2018-02-01 23:08
 */
public class ResponseCode {
    /**
     * 成功
     */
    public static final ResponseCode SUCCESS = new ResponseCode(200, "操作成功");

    /**
     * 失败
     */
    public static final ResponseCode FAILED = new ResponseCode(-1, "操作失败");
    public static final ResponseCode SYS_EXCEPTION = new ResponseCode(500, "系统异常");
    public static final ResponseCode PARAM_ILLEGAL = new ResponseCode(400, "参数有误");
    public static final ResponseCode UN_LOGIN = new ResponseCode(401, "未登陆");
    public static final ResponseCode FORBIDDEN = new ResponseCode(403, "无权限");

    @Getter
    @Setter
    private Integer code;

    @Getter
    @Setter
    private String message;

    public ResponseCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
