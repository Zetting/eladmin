package me.zhengjie.base;

import com.github.pagehelper.PageInfo;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 分页数据封装类
 * Created by cailanfresh on 2019/4/19.
 */
public class CommonPage<T> {
    @Getter
    @Setter
    private Long totalElements;
    @Getter
    @Setter
    private List<T> content;

    /**
     * 将PageHelper分页后的list转为分页信息
     */
    public static <T> CommonPage<T> restPage(List<T> list) {
        CommonPage<T> result = new CommonPage<T>();
        PageInfo<T> pageInfo = new PageInfo<T>(list);
        result.setTotalElements(pageInfo.getTotal());
        result.setContent(pageInfo.getList());
        return result;
    }

    /**
     * 将SpringData分页后的list转为分页信息
     */
    public static <T> CommonPage<T> restPage(Page<T> pageInfo) {
        CommonPage<T> result = new CommonPage<T>();
        result.setTotalElements(pageInfo.getTotalElements());
        result.setContent(pageInfo.getContent());
        return result;
    }

}
