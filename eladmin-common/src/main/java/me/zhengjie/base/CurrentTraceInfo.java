package me.zhengjie.base;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * 当前线程信息
 * @author: linzetian
 * @date:2019/6/14
 */
@Data
@Slf4j
public class CurrentTraceInfo {
    private static final ThreadLocal<CurrentTraceInfo> THREAD_LOCAL = new ThreadLocal();
    /**
     * 线程标识
     **/
    private String traceId;

    /**
     * 请求地址
     */
    private String requestURL;


    public static final void set(String traceId,String requestURL) {
        CurrentTraceInfo currentThread = CurrentTraceInfo.get();
        if (StringUtils.isNoneBlank(traceId)) {
            currentThread.setTraceId(traceId);
        }
        currentThread.setRequestURL(requestURL);
        THREAD_LOCAL.remove();
        THREAD_LOCAL.set(currentThread);
    }


    public static final void cleaer() {
        THREAD_LOCAL.remove();
    }

    public static final CurrentTraceInfo get() {
        CurrentTraceInfo currentTraceInfo = THREAD_LOCAL.get();
        if (currentTraceInfo == null) {
            currentTraceInfo = new CurrentTraceInfo();
        }
        return currentTraceInfo;
    }
}
