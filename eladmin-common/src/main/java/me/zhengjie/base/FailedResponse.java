package me.zhengjie.base;

import lombok.Data;

/**
 * 错误是返回的对象
 * 含唯一标识码
 *
 * @author zet
 * @date 2019/4/19
 */
@Data
public class FailedResponse extends Response {
    private String errCode;

    public FailedResponse(String message) {
        super(ResponseCode.FAILED.getCode(), message, null);
        this.errCode = CurrentTraceInfo.get().getTraceId();
    }

    public FailedResponse(long code, String message) {
        super(code, message, null);
        this.errCode = CurrentTraceInfo.get().getTraceId();
    }

}
