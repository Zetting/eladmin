package me.zhengjie.utils;

import cn.hutool.core.util.IdUtil;

public class SnowflakeUtil {

    /** 工作机器ID(0~31) */
    private static long workerId=18;

    /** 数据中心ID(0~31) */
    private static long datacenterId=21;





     public static Long snowId(){
         return IdUtil.createSnowflake(workerId,datacenterId).nextId();
     }

    //==============================Test=============================================
    /** 测试 */
    public static void main(String[] args) {


        System.out.println(snowId());


    }

}
