package me.zhengjie.exception;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import me.zhengjie.base.ResponseCode;

/**
 * 基础异常类
 *
 * @author: linzetian
 * @date:2019/6/26
 */
@Data
@Slf4j
public class ServerException extends RuntimeException {
    private Integer code;
    private String errorMessage;
    private Throwable cause;

    private ServerException() {
    }

    public ServerException(String message) {
        this.setCode(ResponseCode.FAILED.getCode());
        this.setErrorMessage(message);
    }

    public ServerException(ResponseCode code) {
        this.setCode(code.getCode());
        this.setErrorMessage(code.getMessage());
        this.cause = new Throwable(code.getMessage());
    }

    public ServerException(String msg, Throwable e) {
        super(msg);
        this.setCode(ResponseCode.FAILED.getCode());
        this.setErrorMessage(msg);
        this.cause = e;
    }

    public ServerException(ResponseCode businessCode, String errorMessage) {
        this.setCode(businessCode.getCode());
        this.setErrorMessage(errorMessage);
        this.cause = new Throwable(errorMessage);
    }
}
