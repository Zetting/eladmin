-- ----------------------------
-- 新增示例[sample_user]-增删查改菜单和权限
-- ----------------------------
-- 新增菜单
INSERT INTO `sys_menu` (`create_time`, `i_frame`, `name`, `component`, `pid`, `sort`, `icon`, `path`) VALUES (NOW(), 0, '示例管理', 'system/sample/index', '1', '2', 'peoples', 'sample');

-- 管理员分配权限
INSERT INTO `sys_roles_menus` (`menu_id`, `role_id`) VALUES ((SELECT s._id from (SELECT id as _id from sys_menu WHERE name = '示例管理') as s), '1');

-- 新增权限编码
-- all
INSERT INTO `sys_permission` (`alias`, `create_time`, `name`, `pid`) VALUES ('示例管理', NOW(), 'SAMPLEUSER_ALL', (SELECT s._id from (SELECT id as _id from sys_permission WHERE name = 'SYS_ALL') as s));
-- 创建
INSERT INTO `sys_permission` (`alias`, `create_time`, `name`, `pid`) VALUES ('示例创建', NOW(), 'SAMPLEUSER_CREATE', (SELECT s._id from (SELECT id as _id from sys_permission WHERE name = 'SAMPLEUSER_ALL') as s));
-- 删除
INSERT INTO `sys_permission` (`alias`, `create_time`, `name`, `pid`) VALUES ('示例删除', NOW(), 'SAMPLEUSER_DELETE', (SELECT s._id from (SELECT id as _id from sys_permission WHERE name = 'SAMPLEUSER_ALL') as s));
INSERT INTO `sys_permission` (`alias`, `create_time`, `name`, `pid`) VALUES ('示例查询', NOW(), 'SAMPLEUSER_SELECT', (SELECT s._id from (SELECT id as _id from sys_permission WHERE name = 'SAMPLEUSER_ALL') as s));
INSERT INTO `sys_permission` (`alias`, `create_time`, `name`, `pid`) VALUES ('示例修改', NOW(), 'SAMPLEUSER_EDIT', (SELECT s._id from (SELECT id as _id from sys_permission WHERE name = 'SAMPLEUSER_ALL') as s));
