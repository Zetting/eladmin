package me.zhengjie.admin.modules.sample.service;

import me.zhengjie.base.CommonPage;
import com.baomidou.mybatisplus.extension.service.IService;
import me.zhengjie.entity.sample.SampleUser;
import me.zhengjie.admin.modules.sample.contract.request.sample.SampleUserPageRequest;
import me.zhengjie.admin.modules.sample.contract.request.sample.SampleUserCreateRequest;
import me.zhengjie.admin.modules.sample.contract.request.sample.SampleUserUpdateRequest;
import me.zhengjie.admin.modules.sample.contract.response.sample.SampleUserGetResponse;
import me.zhengjie.admin.modules.sample.contract.response.sample.SampleUserPageResponse;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
* 示例-服务类
* @author zet
* @date 2019-07-05
*/
public interface SampleUserService extends IService<SampleUser>{
    /**
    * queryAll 分页
    * @param request
    * @param pageable
    * @return
    */
    CommonPage<SampleUserPageResponse> listPages(SampleUserPageRequest request, Pageable pageable);


    /**
     * findById
     * @param id
     * @return
     */
    SampleUserGetResponse findById(String id);

    /**
     * create
     * @param request
     * @return
     */
    void create(SampleUserCreateRequest request);

    /**
     * update
     * @param request
     */
    void update(SampleUserUpdateRequest request);

    /**
     * delete
     * @param id
     */
    void delete(String id);
}
