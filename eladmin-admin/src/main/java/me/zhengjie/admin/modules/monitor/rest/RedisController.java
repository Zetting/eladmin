package me.zhengjie.admin.modules.monitor.rest;

import me.zhengjie.aop.log.Log;
import me.zhengjie.admin.modules.monitor.domain.vo.RedisVo;
import me.zhengjie.admin.modules.monitor.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import me.zhengjie.base.Response;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author Zheng Jie
 * @date 2018-12-10
 */
@RestController
@RequestMapping("api")
public class RedisController {

    @Autowired
    private RedisService redisService;

    @Log("查询Redis缓存")
    @GetMapping(value = "/redis")
    @PreAuthorize("hasAnyRole('ADMIN','REDIS_ALL','REDIS_SELECT')")
    public Response getRedis(String key, Pageable pageable){
        return  Response.success(redisService.findByKey(key,pageable));
    }

    @Log("删除Redis缓存")
    @DeleteMapping(value = "/redis")
    @PreAuthorize("hasAnyRole('ADMIN','REDIS_ALL','REDIS_DELETE')")
    public Response delete(@RequestBody RedisVo resources){
        redisService.delete(resources.getKey());
        return Response.success();
    }

    @Log("清空Redis缓存")
    @DeleteMapping(value = "/redis/all")
    @PreAuthorize("hasAnyRole('ADMIN','REDIS_ALL','REDIS_DELETE')")
    public Response deleteAll(){
        redisService.flushdb();
        return Response.success();
    }
}
