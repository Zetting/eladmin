package me.zhengjie.admin.modules.sample.controller;

import me.zhengjie.aop.log.Log;
import me.zhengjie.base.Response;
import me.zhengjie.base.CommonPage;
import me.zhengjie.admin.modules.sample.contract.request.sample.SampleUserPageRequest;
import me.zhengjie.admin.modules.sample.contract.request.sample.SampleUserCreateRequest;
import me.zhengjie.admin.modules.sample.contract.request.sample.SampleUserUpdateRequest;
import me.zhengjie.admin.modules.sample.contract.response.sample.SampleUserPageResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.data.domain.Pageable;
import me.zhengjie.admin.modules.sample.service.SampleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
* 示例-控制层
*
* @author zet
* @date 2019-07-05
*/
@RestController
@RequestMapping("sample/sampleUser")
public class SampleUserController {

    @Autowired
    private SampleUserService sampleUserService;

    @GetMapping(value = "/listPages")
    @PreAuthorize("hasAnyRole('ADMIN','SAMPLEUSER_ALL','SAMPLEUSER_SELECT')")
    public Response listPages(SampleUserPageRequest request, Pageable pageable){
        CommonPage<SampleUserPageResponse> pages = sampleUserService.listPages(request, pageable);
        return Response.success(pages);
    }

    @Log("新增示例")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAnyRole('ADMIN','SAMPLEUSER_ALL','SAMPLEUSER_CREATE')")
    public Response create(@RequestBody SampleUserCreateRequest request){
        sampleUserService.create(request);
        return Response.success();
    }

    @Log("修改示例")
    @PostMapping(value = "/update")
    @PreAuthorize("hasAnyRole('ADMIN','SAMPLEUSER_ALL','SAMPLEUSER_EDIT')")
    public Response update(@RequestBody SampleUserUpdateRequest request){
        sampleUserService.update(request);
        return Response.success();
    }


    @Log("删除示例")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','SAMPLEUSER_ALL','SAMPLEUSER_DELETE')")
    public Response delete(@PathVariable String id){
        sampleUserService.delete(id);
        return Response.success();
    }
}
