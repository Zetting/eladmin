package me.zhengjie.admin.modules.system.rest;

import me.zhengjie.aop.log.Log;
import me.zhengjie.admin.modules.system.domain.Permission;
import me.zhengjie.exception.BadRequestException;
import me.zhengjie.admin.modules.system.service.PermissionService;
import me.zhengjie.admin.modules.system.service.dto.CommonQueryCriteria;
import me.zhengjie.admin.modules.system.service.dto.PermissionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import me.zhengjie.base.Response;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Zheng Jie
 * @date 2018-12-03
 */
@RestController
@RequestMapping("api")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    private static final String ENTITY_NAME = "permission";

    /**
     * 返回全部的权限，新增角色时下拉选择
     * @return
     */
    @GetMapping(value = "/permissions/tree")
    @PreAuthorize("hasAnyRole('ADMIN','PERMISSION_ALL','PERMISSION_CREATE','PERMISSION_EDIT','ROLES_SELECT','ROLES_ALL')")
    public Response getTree(){
        return Response.success(permissionService.getPermissionTree(permissionService.findByPid(0L)));
    }

    @Log("查询权限")
    @GetMapping(value = "/permissions")
    @PreAuthorize("hasAnyRole('ADMIN','PERMISSION_ALL','PERMISSION_SELECT')")
    public Response getPermissions(CommonQueryCriteria criteria){
        List<PermissionDTO> permissionDTOS = permissionService.queryAll(criteria);
        return Response.success(permissionService.buildTree(permissionDTOS));
    }

    @Log("新增权限")
    @PostMapping(value = "/permissions")
    @PreAuthorize("hasAnyRole('ADMIN','PERMISSION_ALL','PERMISSION_CREATE')")
    public Response create(@Validated @RequestBody Permission resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        return Response.success(permissionService.create(resources));
    }

    @Log("修改权限")
    @PutMapping(value = "/permissions")
    @PreAuthorize("hasAnyRole('ADMIN','PERMISSION_ALL','PERMISSION_EDIT')")
    public Response update(@Validated(Permission.Update.class) @RequestBody Permission resources){
        permissionService.update(resources);
        return  Response.success();
    }

    @Log("删除权限")
    @DeleteMapping(value = "/permissions/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','PERMISSION_ALL','PERMISSION_DELETE')")
    public Response delete(@PathVariable Long id){
        permissionService.delete(id);
        return Response.success();
    }
}
