package me.zhengjie.admin.modules.system.service.mapper;

import me.zhengjie.mapper.EntityMapper;
import me.zhengjie.admin.modules.system.domain.Role;
import me.zhengjie.admin.modules.system.service.dto.RoleSmallDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author Zheng Jie
 * @date 2019-5-23
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RoleSmallMapper extends EntityMapper<RoleSmallDTO, Role> {

}
