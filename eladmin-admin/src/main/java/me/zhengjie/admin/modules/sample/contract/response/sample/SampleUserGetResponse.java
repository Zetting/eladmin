package me.zhengjie.admin.modules.sample.contract.response.sample;

import lombok.Data;
import javax.persistence.*;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.io.Serializable;

/**
* 示例-获取响应
* @author zet
* @date 2019-07-05
*/
@Data
public class SampleUserGetResponse {

    @ApiModelProperty("主键Id")
    private String id;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("更新时间")
    private Date updateTime;

    @ApiModelProperty("描述")
    private String description;

    @ApiModelProperty("详细描述")
    private String detailDescription;

    @ApiModelProperty("用户名")
    private String username;
}
