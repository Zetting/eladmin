package me.zhengjie.admin.modules.sample.contract.request.sample;

import lombok.Data;
import javax.persistence.*;
import io.swagger.annotations.ApiModelProperty;
import me.zhengjie.base.BaseRequest;
import java.util.Date;
import java.io.Serializable;

/**
* 示例-创建请求
* @author zet
* @date 2019-07-05
*/
@Data
public class SampleUserCreateRequest extends BaseRequest {


    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("更新时间")
    private Date updateTime;

    @ApiModelProperty("描述")
    private String description;

    @ApiModelProperty("详细描述")
    private String detailDescription;

    @ApiModelProperty("用户名")
    private String username;
}
