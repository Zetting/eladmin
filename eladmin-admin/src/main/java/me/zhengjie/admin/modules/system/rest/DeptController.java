package me.zhengjie.admin.modules.system.rest;

import me.zhengjie.aop.log.Log;
import me.zhengjie.base.Response;
import me.zhengjie.admin.configureration.DataScope;
import me.zhengjie.exception.BadRequestException;
import me.zhengjie.admin.modules.system.domain.Dept;
import me.zhengjie.admin.modules.system.service.DeptService;
import me.zhengjie.admin.modules.system.service.dto.DeptDTO;
import me.zhengjie.admin.modules.system.service.dto.DeptQueryCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
* @author Zheng Jie
* @date 2019-03-25
*/
@RestController
@RequestMapping("api")
public class DeptController {

    @Autowired
    private DeptService deptService;

    @Autowired
    private DataScope dataScope;

    private static final String ENTITY_NAME = "dept";

    @Log("查询部门")
    @GetMapping(value = "/dept")
    @PreAuthorize("hasAnyRole('ADMIN','USER_ALL','USER_SELECT','DEPT_ALL','DEPT_SELECT')")
    public Response getDepts(DeptQueryCriteria criteria){
        // 数据权限
        criteria.setIds(dataScope.getDeptIds());
        List<DeptDTO> deptDTOS = deptService.queryAll(criteria);
        return Response.success(deptService.buildTree(deptDTOS));
    }

    @Log("新增部门")
    @PostMapping(value = "/dept")
    @PreAuthorize("hasAnyRole('ADMIN','DEPT_ALL','DEPT_CREATE')")
    public Response create(@Validated @RequestBody Dept resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        return Response.success(deptService.create(resources));
    }

    @Log("修改部门")
    @PutMapping(value = "/dept")
    @PreAuthorize("hasAnyRole('ADMIN','DEPT_ALL','DEPT_EDIT')")
    public Response update(@Validated(Dept.Update.class) @RequestBody Dept resources){
        deptService.update(resources);
        return  Response.success();
    }

    @Log("删除部门")
    @DeleteMapping(value = "/dept/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','DEPT_ALL','DEPT_DELETE')")
    public Response delete(@PathVariable Long id){
        deptService.delete(id);
        return Response.success();
    }
}
