package me.zhengjie.admin.modules.sample.dao;

import me.zhengjie.admin.modules.sample.contract.request.sample.SampleUserPageRequest;
import me.zhengjie.admin.modules.sample.contract.response.sample.SampleUserPageResponse;
import me.zhengjie.entity.sample.SampleUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;


/**
* 示例-持久层
*
* @author zet
* @date 2019-07-05
*/
public interface SampleUserDao extends BaseMapper<SampleUser> {
    List<SampleUserPageResponse> listPages(SampleUserPageRequest query);
}
