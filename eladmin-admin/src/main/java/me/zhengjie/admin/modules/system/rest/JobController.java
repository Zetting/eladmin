package me.zhengjie.admin.modules.system.rest;

import me.zhengjie.aop.log.Log;
import me.zhengjie.admin.configureration.DataScope;
import me.zhengjie.exception.BadRequestException;
import me.zhengjie.admin.modules.system.domain.Job;
import me.zhengjie.admin.modules.system.service.JobService;
import me.zhengjie.admin.modules.system.service.dto.JobQueryCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import me.zhengjie.base.Response;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
* @author Zheng Jie
* @date 2019-03-29
*/
@RestController
@RequestMapping("api")
public class JobController {

    @Autowired
    private JobService jobService;

    @Autowired
    private DataScope dataScope;

    private static final String ENTITY_NAME = "job";

    @Log("查询岗位")
    @GetMapping(value = "/job")
    @PreAuthorize("hasAnyRole('ADMIN','USERJOB_ALL','USERJOB_SELECT','USER_ALL','USER_SELECT')")
    public Response getJobs(JobQueryCriteria criteria,
                                  Pageable pageable){
        // 数据权限
        criteria.setDeptIds(dataScope.getDeptIds());
        return Response.success(jobService.queryAll(criteria, pageable));
    }

    @Log("新增岗位")
    @PostMapping(value = "/job")
    @PreAuthorize("hasAnyRole('ADMIN','USERJOB_ALL','USERJOB_CREATE')")
    public Response create(@Validated @RequestBody Job resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        return Response.success(jobService.create(resources));
    }

    @Log("修改岗位")
    @PutMapping(value = "/job")
    @PreAuthorize("hasAnyRole('ADMIN','USERJOB_ALL','USERJOB_EDIT')")
    public Response update(@Validated(Job.Update.class) @RequestBody Job resources){
        jobService.update(resources);
        return  Response.success();
    }

    @Log("删除岗位")
    @DeleteMapping(value = "/job/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','USERJOB_ALL','USERJOB_DELETE')")
    public Response delete(@PathVariable Long id){
        jobService.delete(id);
        return Response.success();
    }
}
