package me.zhengjie.admin.modules.monitor.rest;

import me.zhengjie.base.Response;
import me.zhengjie.admin.modules.monitor.service.VisitsService;
import me.zhengjie.utils.RequestHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Zheng Jie
 * @date 2018-12-13
 */
@RestController
@RequestMapping("api")
public class VisitsController {

    @Autowired
    private VisitsService visitsService;

    @PostMapping(value = "/visits")
    public Response create(){
        visitsService.count(RequestHolder.getHttpServletRequest());
        return Response.success();
    }

    @GetMapping(value = "/visits")
    public Response get(){
        return Response.success(visitsService.get());
    }

    @GetMapping(value = "/visits/chartData")
    public Response getChartData(){
        return Response.success(visitsService.getChartData());
    }
}
