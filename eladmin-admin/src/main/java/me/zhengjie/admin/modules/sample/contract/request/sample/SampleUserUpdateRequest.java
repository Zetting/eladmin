package me.zhengjie.admin.modules.sample.contract.request.sample;

import lombok.Data;
import javax.persistence.*;
import io.swagger.annotations.ApiModelProperty;
import me.zhengjie.base.BaseRequest;
import java.util.Date;
import java.io.Serializable;

/**
* 示例-更新请求
* @author zet
* @date 2019-07-05
*/
@Data
public class SampleUserUpdateRequest extends BaseRequest {

    @ApiModelProperty("主键Id")
    private String id;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("更新时间")
    private Date updateTime;

    @ApiModelProperty("描述")
    private String description;

    @ApiModelProperty("详细描述")
    private String detailDescription;

    @ApiModelProperty("用户名")
    private String username;
}
