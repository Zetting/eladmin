package me.zhengjie.admin.modules.sample.service.impl;

import me.zhengjie.base.CommonPage;
import com.github.pagehelper.PageHelper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.zhengjie.entity.sample.SampleUser;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.admin.modules.sample.service.SampleUserService;
import me.zhengjie.admin.modules.sample.dao.SampleUserDao;
import me.zhengjie.admin.modules.sample.contract.request.sample.SampleUserPageRequest;
import me.zhengjie.admin.modules.sample.contract.request.sample.SampleUserCreateRequest;
import me.zhengjie.admin.modules.sample.contract.request.sample.SampleUserUpdateRequest;
import me.zhengjie.admin.modules.sample.contract.response.sample.SampleUserGetResponse;
import me.zhengjie.admin.modules.sample.contract.response.sample.SampleUserPageResponse;
import java.util.Optional;
import java.util.List;
import cn.hutool.core.util.IdUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
import java.util.List;

/**
* 示例-服务实现类
*
* @author zet
* @date 2019-07-05
*/
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class SampleUserServiceImpl extends ServiceImpl<SampleUserDao, SampleUser> implements SampleUserService {
    @Autowired
    private SampleUserDao  sampleUserDao;

    @Override
    public CommonPage<SampleUserPageResponse>  listPages(SampleUserPageRequest request, Pageable pageable){
        PageHelper.startPage(pageable.getPageNumber(), pageable.getPageSize());
        List<SampleUserPageResponse> list = sampleUserDao.listPages(request);
        return CommonPage.restPage(list);
    }

    @Override
    public SampleUserGetResponse findById(String id) {
        SampleUserGetResponse response = new SampleUserGetResponse();
        SampleUser sampleUser =  sampleUserDao.selectById(id);
        BeanUtils.copyProperties(sampleUser, response);
        return response;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(SampleUserCreateRequest request) {
        SampleUser sampleUser = new SampleUser();
        BeanUtils.copyProperties(request, sampleUser);
        sampleUserDao.insert(sampleUser);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(SampleUserUpdateRequest request) {
        SampleUser sampleUser = new SampleUser();
        BeanUtils.copyProperties(request, sampleUser);
        sampleUserDao.updateById(sampleUser);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String id) {
        sampleUserDao.deleteById(id);
    }
}
