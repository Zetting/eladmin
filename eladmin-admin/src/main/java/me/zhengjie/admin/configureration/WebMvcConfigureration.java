package me.zhengjie.admin.configureration;

import me.zhengjie.action.interceptor.LogInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


/**
 * WebMvcConfigurer
 *
 * @author jie
 * @date 2018-11-30
 */
@Configuration
@EnableWebMvc
public class WebMvcConfigureration implements WebMvcConfigurer {
    @Autowired
    private LogInterceptor logInterceptor;


    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowCredentials(true)
                .allowedHeaders("*")
                .allowedOrigins("*")
                .allowedMethods("GET", "POST", "PUT", "DELETE");

    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/META-INF/resources/")
                .setCachePeriod(0);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(logInterceptor);
    }


    private void addRegistryAndExcludePathPatterns(InterceptorRegistry registry, HandlerInterceptorAdapter interceptorAdapter) {
        registry.addInterceptor(interceptorAdapter)
                .addPathPatterns("/**")
                .excludePathPatterns("/swagger-resources/**", "/webjars/**", "/v2/**", "/swagger-ui.html/**");
    }

}
