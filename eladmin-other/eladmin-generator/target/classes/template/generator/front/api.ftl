import request from '@/utils/request'

export function add(data) {
  return request({
    url: '${secondModuleName}/${changeClassName}/create',
    method: 'post',
    data
  })
}

export function del(${pkChangeColName}) {
  return request({
    url: '${secondModuleName}/${changeClassName}/delete/' + ${pkChangeColName},
    method: 'get'
  })
}

export function edit(data) {
  return request({
    url: '${secondModuleName}/${changeClassName}/update',
    method: 'post',
    data
  })
}
