package ${package}.service.impl;

import me.zhengjie.base.CommonPage;
import com.github.pagehelper.PageHelper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ${entityPackage}.${secondModuleName}.${className};
import me.zhengjie.utils.ValidationUtil;
import ${package}.service.${className}Service;
import ${package}.dao.${className}Dao;
<#if genMode = 'admin'>
import ${package}.contract.request.${secondModuleName}.${className}PageRequest;
import ${package}.contract.request.${secondModuleName}.${className}CreateRequest;
import ${package}.contract.request.${secondModuleName}.${className}UpdateRequest;
import ${package}.contract.response.${secondModuleName}.${className}GetResponse;
import ${package}.contract.response.${secondModuleName}.${className}PageResponse;
import java.util.Optional;
import java.util.List;
import cn.hutool.core.util.IdUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
</#if>
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;
import java.util.List;

/**
* ${tableComment}-服务实现类
*
* @author ${author}
* @date ${date}
*/
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ${className}ServiceImpl extends ServiceImpl<${className}Dao, ${className}> implements ${className}Service {
<#if genMode = 'admin'>
    @Autowired
    private ${className}Dao  ${changeClassName}Dao;

    @Override
    public CommonPage<${className}PageResponse>  listPages(${className}PageRequest request, Pageable pageable){
        PageHelper.startPage(pageable.getPageNumber(), pageable.getPageSize());
        List<${className}PageResponse> list = ${changeClassName}Dao.listPages(request);
        return CommonPage.restPage(list);
    }

    @Override
    public ${className}GetResponse findById(${pkColumnType} ${pkChangeColName}) {
        ${className}GetResponse response = new ${className}GetResponse();
        ${className} ${changeClassName} =  ${changeClassName}Dao.selectById(id);
        BeanUtils.copyProperties(${changeClassName}, response);
        return response;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(${className}CreateRequest request) {
        ${className} ${changeClassName} = new ${className}();
        BeanUtils.copyProperties(request, ${changeClassName});
        ${changeClassName}Dao.insert(${changeClassName});
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(${className}UpdateRequest request) {
        ${className} ${changeClassName} = new ${className}();
        BeanUtils.copyProperties(request, ${changeClassName});
        ${changeClassName}Dao.updateById(${changeClassName});
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(${pkColumnType} ${pkChangeColName}) {
        ${changeClassName}Dao.deleteById(${pkChangeColName});
    }
</#if>
}
