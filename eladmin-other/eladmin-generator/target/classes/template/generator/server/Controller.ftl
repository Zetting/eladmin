package ${package}.controller;

<#if genMode = 'api'>
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
</#if>
<#if genMode = 'admin'>
import me.zhengjie.aop.log.Log;
import me.zhengjie.base.Response;
import me.zhengjie.base.CommonPage;
import ${package}.contract.request.${secondModuleName}.${className}PageRequest;
import ${package}.contract.request.${secondModuleName}.${className}CreateRequest;
import ${package}.contract.request.${secondModuleName}.${className}UpdateRequest;
import ${package}.contract.response.${secondModuleName}.${className}PageResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.data.domain.Pageable;
</#if>
import ${package}.service.${className}Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
* ${tableComment}-控制层
*
* @author ${author}
* @date ${date}
*/
<#if genMode = 'api'>
@Api(tags = "${tableComment}")
</#if>
@RestController
@RequestMapping("${secondModuleName}/${changeClassName}")
public class ${className}Controller {

    @Autowired
    private ${className}Service ${changeClassName}Service;

<#if genMode = 'admin'>
    @GetMapping(value = "/listPages")
    @PreAuthorize("hasAnyRole('ADMIN','${upperCaseClassName}_ALL','${upperCaseClassName}_SELECT')")
    public Response listPages(${className}PageRequest request, Pageable pageable){
        CommonPage<${className}PageResponse> pages = ${changeClassName}Service.listPages(request, pageable);
        return Response.success(pages);
    }

    @Log("新增${tableComment}")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAnyRole('ADMIN','${upperCaseClassName}_ALL','${upperCaseClassName}_CREATE')")
    public Response create(@RequestBody ${className}CreateRequest request){
        ${changeClassName}Service.create(request);
        return Response.success();
    }

    @Log("修改${tableComment}")
    @PostMapping(value = "/update")
    @PreAuthorize("hasAnyRole('ADMIN','${upperCaseClassName}_ALL','${upperCaseClassName}_EDIT')")
    public Response update(@RequestBody ${className}UpdateRequest request){
        ${changeClassName}Service.update(request);
        return Response.success();
    }


    @Log("删除${tableComment}")
    @GetMapping(value = "/delete/{${pkChangeColName}}")
    @PreAuthorize("hasAnyRole('ADMIN','${upperCaseClassName}_ALL','${upperCaseClassName}_DELETE')")
    public Response delete(@PathVariable ${pkColumnType} ${pkChangeColName}){
        ${changeClassName}Service.delete(${pkChangeColName});
        return Response.success();
    }
</#if>
}
