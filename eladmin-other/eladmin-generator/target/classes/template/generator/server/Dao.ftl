package ${package}.dao;

<#if genMode = 'admin'>
import ${package}.contract.request.${secondModuleName}.${className}PageRequest;
import ${package}.contract.response.${secondModuleName}.${className}PageResponse;
</#if>
import ${entityPackage}.${secondModuleName}.${className};
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;


/**
* ${tableComment}-持久层
*
* @author ${author}
* @date ${date}
*/
public interface ${className}Dao extends BaseMapper<${className}> {
<#if genMode = 'admin'>
    List<${className}PageResponse> listPages(${className}PageRequest query);
</#if>
}
