package ${package}.contract.response.${secondModuleName};

import lombok.Data;
import javax.persistence.*;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
<#if hasBigDecimal>
import java.math.BigDecimal;
</#if>
import java.io.Serializable;

/**
* ${tableComment}-获取响应
* @author ${author}
* @date ${date}
*/
@Data
public class ${className}GetResponse {
<#if columns??>
    <#list columns as column>

    @ApiModelProperty("${column.columnComment}")
    private ${column.columnType} ${column.changeColumnName};
    </#list>
</#if>
}
