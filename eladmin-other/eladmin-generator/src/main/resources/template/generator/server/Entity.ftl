package ${entityPackage}.${secondModuleName};

import lombok.Data;
import javax.persistence.*;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
<#if hasBigDecimal>
import java.math.BigDecimal;
</#if>
import java.io.Serializable;

/**
* ${tableComment}-实体
* @author ${author}
* @date ${date}
*/
@Data
@Table(name="${tableName}")
public class ${className} implements Serializable {
<#if columns??>
    <#list columns as column>

    <#if column.columnKey = 'PRI'>
    @Id
    <#if auto>
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    </#if>
    </#if>
    <#if column.columnComment != ''>
    @ApiModelProperty("${column.columnComment}")
    </#if>
    private ${column.columnType} ${column.changeColumnName};
    </#list>
</#if>
}
