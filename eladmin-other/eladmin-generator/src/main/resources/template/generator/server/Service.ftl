package ${package}.service;

import me.zhengjie.base.CommonPage;
import com.baomidou.mybatisplus.extension.service.IService;
import ${entityPackage}.${secondModuleName}.${className};
<#if genMode = 'admin'>
import ${package}.contract.request.${secondModuleName}.${className}PageRequest;
import ${package}.contract.request.${secondModuleName}.${className}CreateRequest;
import ${package}.contract.request.${secondModuleName}.${className}UpdateRequest;
import ${package}.contract.response.${secondModuleName}.${className}GetResponse;
import ${package}.contract.response.${secondModuleName}.${className}PageResponse;
</#if>
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
* ${tableComment}-服务类
* @author ${author}
* @date ${date}
*/
public interface ${className}Service extends IService<${className}>{
<#if genMode = 'admin'>
    /**
    * queryAll 分页
    * @param request
    * @param pageable
    * @return
    */
    CommonPage<${className}PageResponse> listPages(${className}PageRequest request, Pageable pageable);


    /**
     * findById
     * @param ${pkChangeColName}
     * @return
     */
    ${className}GetResponse findById(${pkColumnType} ${pkChangeColName});

    /**
     * create
     * @param request
     * @return
     */
    void create(${className}CreateRequest request);

    /**
     * update
     * @param request
     */
    void update(${className}UpdateRequest request);

    /**
     * delete
     * @param ${pkChangeColName}
     */
    void delete(${pkColumnType} ${pkChangeColName});
</#if>
}
