<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="${package}.dao.${className}Dao">
    <#if genMode = 'admin'>
    <select id="listPages" resultType="${package}.contract.response.${secondModuleName}.${className}PageResponse">
        select * from ${tableName} where 1=1
<#if queryColumns??>
   <#list queryColumns as column>
     <#if column.columnQuery = '1'>
         <if test="${column.changeColumnName} != null">
         and ${column.underScoreCaseColumnName} like concat("%",<#noparse>#{</#noparse>${column.changeColumnName}<#noparse>}</#noparse>,"%")
         </if>
    </#if>
    <#if column.columnQuery = '2'>
        <if test="${column.changeColumnName} != null">
        and ${column.underScoreCaseColumnName} = <#noparse>#{</#noparse>${column.changeColumnName}<#noparse>}</#noparse>
        </if>
    </#if>
   </#list>
</#if>
    </select>
    </#if>
</mapper>
