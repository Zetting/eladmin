package ${package}.contract.request.${secondModuleName};

import lombok.Data;
import javax.persistence.*;
import io.swagger.annotations.ApiModelProperty;
import me.zhengjie.base.BaseRequest;
import java.util.Date;
<#if hasBigDecimal>
import java.math.BigDecimal;
</#if>
import java.io.Serializable;

/**
* ${tableComment}-创建请求
* @author ${author}
* @date ${date}
*/
@Data
public class ${className}CreateRequest extends BaseRequest {
<#if columns??>
    <#list columns as column>

   <#if column.columnKey != 'PRI'>
    @ApiModelProperty("${column.columnComment}")
    private ${column.columnType} ${column.changeColumnName};
   </#if>
    </#list>
</#if>
}
