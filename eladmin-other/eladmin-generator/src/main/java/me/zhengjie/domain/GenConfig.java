package me.zhengjie.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * 代码生成配置
 *
 * @author Zheng Jie
 * @date 2019-01-03
 */
@Data
@Entity
@Table(name = "basic_gen_config")
public class GenConfig {

    @Id
    private Long id;

    /**
     * 包路径
     **/
    private String pack;

    /**
     * 模块名
     **/
    @Column(name = "module_name")
    private String moduleName;

    /**
     * 前端文件路径
     **/
    private String path;

    /**
     * 前端文件路径
     **/
    @Column(name = "api_path")
    private String apiPath;

    /**
     * 作者
     **/
    private String author;

    /**
     * 表前缀
     **/
    private String prefix;

    /**
     * 是否覆盖
     **/
    private Boolean cover;

    /**
     * 服务端模板
     **/
    @Column(name = "server_temples")
    private String serverTemples;

    /**
     * 前端端模板
     **/
    @Column(name = "front_temples")
    private String frontTemples;

    /**
     * 生成模式；admin：后台管理模式，Api：Api模式，详细见备注
     **/
    @Column(name = "gen_mode")
    private String genMode;

    /**
     * 备注
     **/
    private String remark;
}
