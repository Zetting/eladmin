package me.zhengjie.rest;

import cn.hutool.core.util.PageUtil;
import me.zhengjie.domain.vo.ColumnInfo;
import me.zhengjie.domain.vo.TableInfo;
import me.zhengjie.exception.BadRequestException;
import me.zhengjie.service.GenConfigService;
import me.zhengjie.service.GeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import me.zhengjie.base.Response;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Zheng Jie
 * @date 2019-01-02
 */
@RestController
@RequestMapping("api")
public class GeneratorController {

    @Autowired
    private GeneratorService generatorService;

    @Autowired
    private GenConfigService genConfigService;

    @Value("${generator.enabled}")
    private Boolean generatorEnabled;

    /**
     * 查询数据库元数据
     *
     * @param name
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = "/generator/tables")
    public Response getTables(@RequestParam(defaultValue = "") String name,
                              @RequestParam(defaultValue = "0") Integer page,
                              @RequestParam(defaultValue = "10") Integer size) {
        int[] startEnd = PageUtil.transToStartEnd(page + 1, size);
        return Response.success(generatorService.getTables(name, startEnd));
    }

    /**
     * 查询表内元数据
     *
     * @param tableName
     * @return
     */
    @GetMapping(value = "/generator/columns")
    public Response getTables(@RequestParam String tableName) {
        return Response.success(generatorService.getColumns(tableName));
    }

    /**
     * 生成代码
     *
     * @param columnInfos
     * @return
     */
    @PostMapping(value = "/generator")
    public Response generator(@RequestBody List<ColumnInfo> columnInfos,
                              @RequestParam String tableName) {
        if (!generatorEnabled) {
            throw new BadRequestException("此环境不允许生成代码！");
        }
        TableInfo tableInfo = generatorService.getTableInfo(tableName);
        generatorService.generator(columnInfos, genConfigService.find(), tableInfo);
        return Response.success();
    }

}
