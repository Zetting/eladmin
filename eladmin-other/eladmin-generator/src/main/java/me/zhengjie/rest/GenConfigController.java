package me.zhengjie.rest;

import me.zhengjie.domain.GenConfig;
import me.zhengjie.service.GenConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import me.zhengjie.base.Response;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author Zheng Jie
 * @date 2019-01-14
 */
@RestController
@RequestMapping("api")
public class GenConfigController {

    @Autowired
    private GenConfigService genConfigService;

    /**
     * 查询生成器配置
     *
     * @return
     */
    @GetMapping(value = "/genConfig")
    public Response get() {
        return Response.success(genConfigService.find());
    }

    @PutMapping(value = "/genConfig")
    public Response genConfig(@Validated @RequestBody GenConfig genConfig) {
        return Response.success(genConfigService.update(genConfig));
    }
}
