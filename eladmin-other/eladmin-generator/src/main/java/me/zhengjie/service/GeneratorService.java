package me.zhengjie.service;

import me.zhengjie.domain.GenConfig;
import me.zhengjie.domain.vo.ColumnInfo;
import me.zhengjie.domain.vo.TableInfo;

import java.util.List;
import java.util.Map;

/**
 * @author Zheng Jie
 * @date 2019-01-02
 */
public interface GeneratorService {
    /**
     * 获取表信息
     *
     * @param tableName
     * @return
     */
    TableInfo getTableInfo(String tableName);

    /**
     * 查询数据库元数据
     *
     * @param name
     * @param startEnd
     * @return
     */
    Object getTables(String name, int[] startEnd);

    /**
     * 得到数据表的元数据
     *
     * @param name
     * @return
     */
    Object getColumns(String name);

    /**
     * 生成代码
     *
     * @param columnInfos
     * @param genConfig
     * @param tableInfo
     */
    void generator(List<ColumnInfo> columnInfos, GenConfig genConfig, TableInfo tableInfo);
}
