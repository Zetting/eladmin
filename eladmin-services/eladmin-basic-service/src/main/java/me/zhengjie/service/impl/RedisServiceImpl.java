package me.zhengjie.service.impl;

import me.zhengjie.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @author Zheng Jie
 * @date 2018-12-10
 */
@Service
public class RedisServiceImpl implements RedisService {

    @Autowired
    RedisTemplate redisTemplate;

//    @Override
//    public Page<RedisVo> findByKey(String key, Pageable pageable){
//        List<RedisVo> redisVos = new ArrayList<>();
//        if(!key.equals("*")){
//            key = "*" + key + "*";
//        }
//        for (Object s : redisTemplate.keys(key)) {
//            // 过滤掉权限的缓存
//            if (s.toString().indexOf("role::loadPermissionByUser") != -1 || s.toString().indexOf("user::loadUserByUsername") != -1) {
//                continue;
//            }
//            RedisVo redisVo = new RedisVo(s.toString(),redisTemplate.opsForValue().get(s.toString()).toString());
//            redisVos.add(redisVo);
//        }
//        Page<RedisVo> page = new PageImpl<RedisVo>(
//                PageUtil.toPage(pageable.getPageNumber(),pageable.getPageSize(),redisVos),
//                pageable,
//                redisVos.size());
//        return page;
//    }



    @Override
    public void delete(String key) {
        redisTemplate.delete(key);
    }

    @Override
    public void flushdb() {
        redisTemplate.getConnectionFactory().getConnection().flushDb();
    }

    @Override
    public String getCodeVal(String key) {
        try {
            String value = redisTemplate.opsForValue().get(key).toString();
            return value;
        }catch (Exception e){
            return "";
        }
    }

    @Override
    public void saveCode(String key, Object val) {
        redisTemplate.opsForValue().set(key,val,2000);
    }
}
